package cz.pixelmate.radbeacondetection

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_beacon.*
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.utils.UrlBeaconUrlCompressor

class BeaconsRecyclerAdapter : RecyclerView.Adapter<BeaconsRecyclerAdapter.ViewHolder>() {

    var data: List<Beacon> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : BeaconsRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_beacon, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(vh: BeaconsRecyclerAdapter.ViewHolder, position: Int) {
        val beacon = data[position]

        vh.tvMac.text = beacon.bluetoothAddress

        try {
            when (beacon.beaconTypeCode) {
                0x10 -> {
                    vh.tvType.text = "Eddystone URL"
                    vh.tvId1.text = UrlBeaconUrlCompressor.uncompress(beacon.id1.toByteArray())
                    vh.tvId2.text = "-"
                    vh.tvId3.text = "-"
                }
                0xBEAC -> {
                    vh.tvType.text = "AltBeacon"
                    vh.tvId1.text = beacon.id1.toString()
                    vh.tvId2.text = beacon.id2.toString()
                    vh.tvId3.text = beacon.id3.toString()
                }
                0 -> {
                    vh.tvType.text = "Eddystone UID"
                    vh.tvId1.text = beacon.id1.toString()
                    vh.tvId2.text = beacon.id2.toString()
                    vh.tvId3.text = "-"
                }
                else -> {
                    vh.tvType.text = "Unknown"
                    vh.tvId1.text = "-"
                    vh.tvId2.text = "-"
                    vh.tvId3.text = "-"
                }
            }
        } catch (ex: IndexOutOfBoundsException) {
            // Library is getting ids from array without checking their presence first
            ex.printStackTrace()
            vh.tvMac.text = "Error"
        }
    }

    class ViewHolder(override val containerView: View)
        : RecyclerView.ViewHolder(containerView), LayoutContainer

}