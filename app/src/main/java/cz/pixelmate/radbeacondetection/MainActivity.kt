package cz.pixelmate.radbeacondetection

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.RemoteException
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.altbeacon.beacon.*

class MainActivity : AppCompatActivity(), BeaconConsumer {

    private lateinit var beaconManager: BeaconManager
    private lateinit var statusRecyclerAdapter: StatusRecyclerAdapter
    private lateinit var beaconsRecyclerAdapter: BeaconsRecyclerAdapter

    companion object {
        const val ALTBEACON_LAYOUT = "m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
        const val EDDYSTONE_UID_LAYOUT = "s:0-1=feaa,m:2-2=00,p:3-3:-41,i:4-13,i:14-19"
        const  val EDDYSTONE_URL_LAYOUT = "s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-21v"
        const val IBEACON_LAYOUT = "m:0-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!hasRequiredPermissions()) {
            requestPermissions()
        }

        prepareStatusRecyclerView()
        prepareBeaconsRecyclerView()

        beaconManager = BeaconManager.getInstanceForApplication(this)
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(ALTBEACON_LAYOUT))
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(EDDYSTONE_URL_LAYOUT))
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(EDDYSTONE_UID_LAYOUT))
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(IBEACON_LAYOUT))
        beaconManager.bind(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        beaconManager.unbind(this)
    }

    private fun hasRequiredPermissions(): Boolean {
        return (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 0)
    }

    private fun prepareStatusRecyclerView() {
        statusRecyclerAdapter = StatusRecyclerAdapter()
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.reverseLayout = true
        rvStatusMessages.layoutManager = layoutManager
        rvStatusMessages.adapter = statusRecyclerAdapter
    }

    private fun prepareBeaconsRecyclerView() {
        beaconsRecyclerAdapter = BeaconsRecyclerAdapter()
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rvBeacons.layoutManager = layoutManager
        rvBeacons.adapter = beaconsRecyclerAdapter
    }

    private fun addStatusMessage(message: String) {
        statusRecyclerAdapter.pushListItem(message)
    }

    private fun showNearbyBeacons(beacons: List<Beacon>) {
        beaconsRecyclerAdapter.data = beacons
    }

    /* ---------- BeaconConsumer ---------- */

    override fun onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(object : MonitorNotifier {

            override fun didDetermineStateForRegion(p0: Int, p1: Region?) {
                addStatusMessage("Region state: $p0")
            }

            override fun didEnterRegion(p0: Region?) {
                addStatusMessage("Device entered beacon's region")
            }

            override fun didExitRegion(p0: Region?) {
                addStatusMessage("Device exited beacon's region")
            }
        })

        try {
            beaconManager.startMonitoringBeaconsInRegion(
                    Region("MonitoringRegionId", null, null, null))
        } catch (e: RemoteException) {
            e.printStackTrace()
        }

        beaconManager.addRangeNotifier { p0, _ -> p0?.let { showNearbyBeacons(it.toList()) } }

        beaconManager.startRangingBeaconsInRegion(
                Region("RangeRegionId", null, null, null))
    }
}
