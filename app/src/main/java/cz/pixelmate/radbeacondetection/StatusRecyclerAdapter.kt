package cz.pixelmate.radbeacondetection

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_status.*

class StatusRecyclerAdapter : RecyclerView.Adapter<StatusRecyclerAdapter.ViewHolder>() {

    val data: MutableList<String> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : StatusRecyclerAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_status, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(vh: StatusRecyclerAdapter.ViewHolder, position: Int) {
        vh.tvText.text = data[position]
    }

    fun pushListItem(message: String) {
        data.add(0, message)
        notifyDataSetChanged()
    }

    class ViewHolder(override val containerView: View)
        : RecyclerView.ViewHolder(containerView), LayoutContainer
}