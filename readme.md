# RadBeacon detection

Projekt slouží jako příklad aplikace detekující přítomnost BLE Beaconu RadBeacon Dot od společnosti Radius Networks.

## Popis aplikace

Aplikace obsahuje pouze jednu obrazovku se dvěma listy. První list "Status changes"
popisuje aktuální status zařízení (zda se nachází v okolí některého z beaconů) a historii změn tohoto statusu.

Druhý list "Detected beacons" obsahuje seznam aktuálně detekovaných beaconů v okolí zařízení. RadBeacon je
schopný emitovat různé formáty dat (konkrétně Alt, iBeacon a Eddystone URL/UID), z toho důvodu je
možné, že se jeden beacon (jednoznačně identifikovaný pomocí MAC adresy) může v seznamu zobrazit
vícekrát s různými daty a různým typem.

> *Note:* Formát dat typu iBeacon se bohužel nepodařilo detekovat. Možnou příčinou může být
> neodpovídající bluetooth layout používaný pro filtraci zařízení.

## Spuštění aplikace

Po standardním sbuildování by aplikace měla být plně spustitelná a měla by začít detekovat okolní
beacony, jediné, co vyžaduje, je povolení přístupu k poloze zařízení a zapnutý bluetooth modul.

## Konfigurace beaconu

Beacon je nutné před použitím nakonfigurovat. Společnost RadNetwork k tomu poskytuje aplikaci
s názvem [RadBeacon](https://play.google.com/store/apps/details?id=com.radiusnetworks.radbeacon&hl=en) 
stáhnutelnou běžnou cestou z Google Play Store. Před konfigurací je nutné přepnout
beacon do konfiguračního módu (v zapnutém stavu podržet tlačítko dokud beacon zeleně nezabliká).
V konfiguraci je možné upravovat vysílané typy datových formátů a dat které nesou. Před nahráním
nastavené konfigurace do beaconu je nutné zadat pin (defaultně "00000000”).
